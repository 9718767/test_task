# Тестовое задание для Astra Linux

## Ключевые особенности:
  - Собран на базе generic/ubuntu2004
  - Vagrant запускает машину с 1024 мб ОЗУ и 2 ядра ЦПУ
  - Ansible устанавливается через pip3
  - Чувствительные данные вынесены в vars
  - Ansible запускает 2 роли: docker и node_exporter
  - node_exporter ставится из [исходников](https://github.com/prometheus/node_exporter/releases/download/v1.3.1/node_exporter-1.3.1.linux-amd64.tar.gz)
  - docker и docker-compose устанавливается из репозитория
  - В конце Ansible запускает docker-compose с 2 ВМ:
    - Grafana 8.2.6
    - Prometheus 2.34.0

## Общий алгоритм запуска
```bash
git clone git@gitlab.com:9718767/test_task.git && \
cd test_task && \
vagrant up
```
Далее заходим по адресу http://localhost:3000 ->  вводим данные от учетной записи admin:admin1234 ->  переходим в борду

## Возможные ошибки и пути решения
Наблюдал плавующую ошибку

```bash
Vagrant assumes that this means the command failed!
 add-apt-repository ppa:ansible/ansible -y && apt-get update -y -qq && \
 DEBIAN_FRONTEND=noninteractive apt-get install -y -qq ansible --option "Dpkg::Options::=--force-confold"
```

Лечилось запуском provision
```bash
vagrant provision
```